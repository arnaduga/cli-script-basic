import sys
import os
import click
import time


@click.command()
@click.option('--env','-e', default="dev", help="Environment context")
@click.option('--org','-o', required=True, help="Organization context")
def myFunction(env, org):

    """This script command will not do something funny, sorry about that"""

    try:
        username = os.environ['APEX_USERNAME']
        password = os.environ['APEX_PASSWORD']
    except Exception as e:
        print("ERROR: APEX_USERNAME and APEX_PASSWORD must be set")
        sys.exit(1)


    click.echo('\nLet\'s do it!\n')
    all = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
    with click.progressbar(all, label="Processing username={}, password={}, org={}, env={}...".format(username,password,org,env)) as bar:
        for x in bar:
            time.sleep(1)


if __name__ == '__main__':
    myFunction()